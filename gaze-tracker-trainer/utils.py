import os, sys, json, datetime, time


def get_timestamp_ISO():
    dt = str(datetime.datetime.now().isoformat()).replace('-','_').replace(':',"_")
    return dt[:-7]

def file_exists(path, min_size_mb=None, allow_empty_files=False):
    try:
        exists = os.path.exists(path)
        if exists:
            size = os.path.getsize(path)/1024*1024
            if size>0:
                if min_size_mb is not None: 
                    if size>=min_size_mb:
                        return True
                else:
                    return True
            elif allow_empty_files:
                return True
        
        return False
    except:
        return False

class DictStorage():
    def __init__(self,file_name) -> None:
        self.file_name = file_name
        self.data = self.load()

    def exists(self,key):
        return True if key in self.data.keys() else False
        
    def update(self, key, value):
        self.data.update({key:value})

    def multiple_update(self,data_dict):
        self.data.update(data_dict)
    
    def retreive(self,key):
        if self.exists(key):
            return self.data[key]
        else:
            return None

    def load(self):
        data = {}
        if file_exists(self.file_name):
            data = json.load(open(self.file_name,'r'))  

        return data
    
    def save(self):
        saved = self.load()
        saved.update(self.data)
        json.dump(saved,open(self.file_name,'w'),indent=4)


def find_all_files_in_folder(folder, extension):
    if os.path.exists(folder):
        paths= []
        check_paths = []
        for path, subdirs, files in os.walk(folder):
            for file_path in files:
                if file_path.endswith(extension) and not file_path.startswith('.'):
                    if not any(file_path in s for s in check_paths):
                        paths.append(os.path.join(path , file_path))
                        check_paths.append(file_path)
        return paths
    else:
        raise Exception("path does not exist -> "+ folder)
